[CHIRP](https://chirp.danplanet.com/projects/chirp/wiki/Home) is a free, open-source tool for programming your amateur radio. [Flatpak](https://flatpak.org) is the future of application distribution across Linux desktops. This CHIRP Flatpak channel is the quickest, easiest way to install and update CHIRP across any Linux distribution that supports Flatpaks.

## Getting Started

The easiest way to install CHIRP is to download the [Flatpak repository file](./index.flatpakrepo). If your browser doesn't open this automatically, you may need to do so manually. CHIRP should then be visible in your list of available software.

Alternatively, run the following from the command line:

```bash
$ flatpak remote-add --if-not-exists --no-gpg-verify chirp https://ndarilek.gitlab.io/chirp/index.flatpakrepo
...
$ flatpak install chirp com.danplanet.chirp.Chirp
```

If you receive permission errors and would rather not install CHIRP as root, add the `--user` flag to the above commands.

## Updating

Either use your normal software center's update mechanisms, or run the following from the command line:

```bash
$ flatpak update
```
